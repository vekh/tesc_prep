
%__________________________Example 6_____________
% Test_Tucker.m
clear;  close all;
max_Tr=10; % maximum Tucker rank
b=10.0; % size of the interval
T_error=zeros(1,max_Tr); nd=3; kmax =3;
coef=1; al=2.0;
n= 64; b2=b/2; h1 = b/n;
x=-b2:h1:b2; [~,n1]=size(x); % interval in x: [-b,b]
y=-b2-h1:h1:b2+h1; [~,n2]=size(y); % interval in y: [-b-h1,b+h1]
z=-b2-2*h1:h1:b2+2*h1; [~,n3]=size(z); % interval in y: [-b-2h1,b+2h1]
A3=zeros(n1,n2,n3); A3F=zeros(n1,n2,n3);
%__________ generate a 3D tensor__________________
for i=1:n1
for j=1:n2
A3(i,j,:)=coef*exp(-al*sqrt(x(1,i)^2 + y(1,j)^2 + z(1,:).^2));
end
end
Ini.U1=zeros(n1,max_Tr); Ini.U2=zeros(n2,max_Tr);
Ini.U3=zeros(n3,max_Tr);
for ir=1:max_Tr
NR=[ir ir ir];
[U1,U2,U3,LAM3F,Ini] = Tucker_full_3D_ini(A3,NR,kmax,ir,Ini);
A3F=Tuck_2_F(LAM3F,U1,U2,U3);
err=TnormF(A3F - A3)/Tnorm(A3);
T_error(1,ir)=abs(err);
fprintf(1, '\n iter = %d , err_Fro = %5.4e \n', ir, err);
end
figure(1); %____ draw convergence of the error____
semilogy(T_error(1,1:max_Tr),'Linewidth',2,'Marker','square');
set(gca,'fontsize',16);
xlabel('Tucker rank','fontsize',16);
ylabel('Frobenius error','fontsize',16);
grid on; axis tight;
%_______________draw the function______________________________
A2=A3F(:,:,(n3-1)/2); % take a 2D plane of the 3D tensor
figure(2); mesh(y,x,A2); axis tight;
%_________________draw__Tucker vectors_________________________
figure(3)
plot(x,U1(:,1),'Linewidth',2);
hold on;
for j=2:max_Tr-2
plot(x,U1(:,j),'Linewidth',2);
end
set(gca,'fontsize',16);
str3='Tucker vectors';
str2=[str3,' al= ',num2str(al) ', rank = ',num2str(max_Tr)];
title(str2,'fontsize',16);  grid on; hold off;
%___________________


 

%-----------------------------------------------
function [U1,U2,U3,LAM3F,Ini] = Tucker_full_3D_ini(A3,NR,kmax,ir,Ini)
[n1,n2,n3]=size(A3);
R1=NR(1); R2=NR(2); R3=NR(3); %nd=3;
[~,nstep]=size(Ini.U1);
if ir == 1
%_______________ Fase I - Initial Guess __________________________
D= permute(A3,[1,3,2]); B1= reshape(D,n1,n2*n3);
[Us, ~, ~]= svd(double(B1),0); U1=Us(:,1:R1);
Ini.U1=Us(:,1:nstep);
Ini.B1=B1;
D= permute(A3,[2,1,3]); B2= reshape(D,n2,n1*n3);
[Us, ~, ~]= svd(double(B2),0); U2=Us(:,1:R2);
Ini.U2=Us(:,1:nstep);
Ini.B2=B2;
D= permute(A3,[3,2,1]); B3= reshape(D,n3,n1*n2);
[Us, ~, ~]= svd(double(B3),0); U3=Us(:,1:R3);
Ini.U3=Us(:,1:nstep);
Ini.B3=B3;
end
if ir ~= 1;
U1=Ini.U1(:,1:R1); U2=Ini.U2(:,1:R2); U3=Ini.U3(:,1:R3);
B1=Ini.B1;
B2=Ini.B2;
B3=Ini.B3;
end
%_______________ Fase II - ALS Iteration ___________________________
for k1=1:kmax
Y1= B1*kron(U2,U3); C1=reshape(Y1,n1,R2*R3);
[W, ~, ~] = svd(double(C1), 0);
U1= W(:,1:R1);
Y2= B2*kron(U3,U1); C2=reshape(Y2,n2,R1*R3);
[W, ~, ~] = svd(double(C2), 0);
U2= W(:,1:R2);
Y3= B3*kron(U1,U2); C3=reshape(Y3,n3,R2*R1);
[W, ~, ~] = svd(double(C3), 0);
U3= W(:,1:R3);
end
Y1= B1*kron(U2,U3); LAM3 = U1'*Y1 ; LLL=reshape(LAM3,R1,R3,R2);
LAM3F=permute(LLL,[1,3,2]);
end 
%_________Example 4______________________________________
function f = TnormF(A)
NS = size(A); nd =length(NS); nsa =1;
for i = 1:nd
nsa = nsa*NS(i);
end
B = reshape(A,1,nsa); f = norm(B);
end
%_________________________________________________________
 
%_________Example 5______________________________________
function A3F=Tuck_2_F(LAM3F,U1,U2,U3)
[R1,R2,R3]=size(LAM3F);
[n1,~]=size(U1); [n2,~]=size(U2); [n3,~]=size(U3);
LAM31=reshape(LAM3F,R1,R2*R3);
CNT1=LAM31'*U1';
CNT2=reshape(CNT1,R2,R3*n1);
CNT3=CNT2'*U2';
CNT4=reshape(CNT3,R3,n1*n2);
CNT5=CNT4'*U3';
A3F=reshape(CNT5,n1,n2,n3);
end
%____________






 